import type { NavigationGuard } from 'vue-router'
export type MiddlewareKey = string
declare module "/home/pc02/Documentos/vitor-dev/crud-laranuxt/frontend/node_modules/nuxt/dist/pages/runtime/composables" {
  interface PageMeta {
    middleware?: MiddlewareKey | NavigationGuard | Array<MiddlewareKey | NavigationGuard>
  }
}